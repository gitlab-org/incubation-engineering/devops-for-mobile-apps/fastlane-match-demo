### Fastlane Match Storage Mode Demo

[What is Fastlane Match?](https://docs.fastlane.tools/actions/match/)

A couple of things to note:

* Currently Project-level Secure Files is only available on gitlab.com.
* Fastlane Match integration isn't in a Fastlane release yet, but can be pulled in from the master branch. `gem 'fastlane', git: 'https://github.com/fastlane/fastlane.git'` 
* Upload commands require a GitLab personal access token ([instructions](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html))


#### Setup Fastlane Match

```
bundle exec fastlane match init
```

```
[✔] 🚀
[11:45:23]: fastlane match supports multiple storage modes, please select the one you want to use:
1. git
2. google_cloud
3. s3
4. gitlab_secure_files
?  4
[11:45:25]: Initializing match for GitLab project
[11:45:25]: What is your GitLab Project (i.e. gitlab-org/gitlab): gitlab-org/incubation-engineering/devops-for-mobile-apps/fastlane-match-demo
[11:45:27]: Successfully created './fastlane/Matchfile'. You can open the file using a code editor.
[11:45:27]: You can now run `fastlane match development`, `fastlane match adhoc`, `fastlane match enterprise` and `fastlane match appstore`
[11:45:27]: On the first run for each environment it will create the provisioning profiles and
[11:45:27]: certificates for you. From then on, it will automatically import the existing profiles.
[11:45:27]: For more information visit https://docs.fastlane.tools/actions/match/
```

#### Generate and Upload

```
PRIVATE_TOKEN=YOUR-TOKEN bundle exec fastlane match 
```

```
[✔] 🚀
[11:42:18]: Successfully loaded 'fastlane-match-demo/fastlane/Matchfile' 📄

+----------------+-------------------------------------------------------------------------+
|                       Detected Values from './fastlane/Matchfile'                        |
+----------------+-------------------------------------------------------------------------+
| gitlab_project | gitlab-org/incubation-engineering/devops-for-mobile-apps/fastlane-matc  |
|                | h-demo                                                                  |
| storage_mode   | gitlab_secure_files                                                     |
| type           | development                                                             |
+----------------+-------------------------------------------------------------------------+


+----------------------------------------+-------------------------------------------------------------------------+
|                                            Summary for match 2.207.0                                             |
+----------------------------------------+-------------------------------------------------------------------------+
| type                                   | development                                                             |
| readonly                               | false                                                                   |
| generate_apple_certs                   | true                                                                    |
| skip_provisioning_profiles             | false                                                                   |
| app_identifier                         | ["com.gitlab.matchDemo"]                                                |
| username                               | dfrey@gitlab.com                                                        |
| team_id                                | N7SYAN8PX8                                                              |
| storage_mode                           | gitlab_secure_files                                                     |
| git_branch                             | master                                                                  |
| shallow_clone                          | false                                                                   |
| clone_branch_directly                  | false                                                                   |
| skip_google_cloud_account_confirmation | false                                                                   |
| gitlab_project                         | gitlab-org/incubation-engineering/devops-for-mobile-apps/fastlane-matc  |
|                                        | h-demo                                                                  |
| keychain_name                          | login.keychain                                                          |
| force                                  | false                                                                   |
| force_for_new_devices                  | false                                                                   |
| include_all_certificates               | false                                                                   |
| force_for_new_certificates             | false                                                                   |
| skip_confirmation                      | false                                                                   |
| safe_remove_certs                      | false                                                                   |
| skip_docs                              | false                                                                   |
| platform                               | ios                                                                     |
| derive_catalyst_app_identifier         | false                                                                   |
| fail_on_name_taken                     | false                                                                   |
| skip_certificate_matching              | false                                                                   |
| skip_set_partition_list                | false                                                                   |
| verbose                                | false                                                                   |
+----------------------------------------+-------------------------------------------------------------------------+

[11:42:18]: Initializing match for GitLab project gitlab-org/incubation-engineering/devops-for-mobile-apps/fastlane-match-demo
[11:42:22]: Verifying that the certificate and profile are still valid on the Dev Portal...
[11:42:24]: Installing certificate...

+-------------------+--------------------------------------------+
|                     Installed Certificate                      |
+-------------------+--------------------------------------------+
| User ID           | N9NDSZF6KZ                                 |
| Common Name       | Apple Development: Darby Frey (A3TN6GLHVV) |
| Organisation Unit | N7SYAN8PX8                                 |
| Organisation      | Darby Frey                                 |
| Country           | US                                         |
| Start Datetime    | 2022-07-13 16:20:01 UTC                    |
| End Datetime      | 2023-07-13 16:20:00 UTC                    |
+-------------------+--------------------------------------------+

[11:42:24]: Installing provisioning profile...

+---------------------+------------------------------------------------+------------------------------------------------+
|                                            Installed Provisioning Profile                                             |
+---------------------+------------------------------------------------+------------------------------------------------+
| Parameter           | Environment Variable                           | Value                                          |
+---------------------+------------------------------------------------+------------------------------------------------+
| App Identifier      |                                                | com.gitlab.matchDemo                           |
| Type                |                                                | development                                    |
| Platform            |                                                | ios                                            |
| Profile UUID        | sigh_com.gitlab.matchDemo_development          | b2332b39-1644-4bae-8962-60b067e2bbe8           |
| Profile Name        | sigh_com.gitlab.matchDemo_development_profile  | match Development com.gitlab.matchDemo         |
|                     | -name                                          |                                                |
| Profile Path        | sigh_com.gitlab.matchDemo_development_profile  | Library/MobileDevice/Provisionin  |
|                     | -path                                          | g                                              |
|                     |                                                | Profiles/b2332b39-1644-4bae-8962-60b067e2bbe8  |
|                     |                                                | .mobileprovision                               |
| Development Team ID | sigh_com.gitlab.matchDemo_development_team-id  | N7SYAN8PX8                                     |
| Certificate Name    | sigh_com.gitlab.matchDemo_development_certifi  |                                                |
|                     | cate-name                                      |                                                |
+---------------------+------------------------------------------------+------------------------------------------------+

[11:42:25]: All required keys, certificates and provisioning profiles are installed 🙌
```

#### Import and Upload

```
PRIVATE_TOKEN=YOUR-TOKEN bundle exec fastlane match import
```

```
[✔] 🚀
[11:37:38]: Successfully loaded 'fastlane-match-demo/fastlane/Matchfile' 📄

+----------------+-------------------------------------------------------------------------+
|                       Detected Values from './fastlane/Matchfile'                        |
+----------------+-------------------------------------------------------------------------+
| gitlab_project | gitlab-org/incubation-engineering/devops-for-mobile-apps/fastlane-matc  |
|                | h-demo                                                                  |
| storage_mode   | gitlab_secure_files                                                     |
| type           | development                                                             |
+----------------+-------------------------------------------------------------------------+

[11:37:38]: Certificate (.cer) path:

[11:37:56]: Private key (.p12) path:

[11:38:25]: Provisioning profile (.mobileprovision or .provisionprofile) path or leave empty to skip this file:

[11:38:36]: Initializing match for GitLab project gitlab-org/incubation-engineering/devops-for-mobile-apps/fastlane-match-demo
[11:38:37]: Repo is at: '/var/folders/zh/bqqty9d97yxbgg9lpd5b6jlh0000gn/T/d20220713-94254-6ghjaw'
[11:38:37]: Verifying that the certificate and profile are still valid on the Dev Portal...
[11:38:40]: Login to App Store Connect (dfrey@gitlab.com)
[11:38:43]: Finished uploading files to GitLab Secure Files Storage [gitlab-org/incubation-engineering/devops-for-mobile-apps/fastlane-match-demo]
```

![Screen_Shot_2022-07-13_at_11.47.19_AM](https://gitlab.com/gitlab-org/incubation-engineering/devops-for-mobile-apps/readme/uploads/252e720cd33bb0e5459cf1006c8717ce/Screen_Shot_2022-07-13_at_11.47.19_AM.png)
